package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MenuEscolar extends AppCompatActivity {
    //TextViews Lunes
    private TextView nombrePlatoEscolarLunes1, nombrePlatoEscolarLunes2, nombrePlatoEscolarLunes3;
    //TextViews Martes
    private TextView nombrePlatoEscolarMartes1, nombrePlatoEscolarMartes2, nombrePlatoEscolarMartes3;
    //TextViews Miercoles
    private TextView nombrePlatoEscolarMiercoles1, nombrePlatoEscolarMiercoles2, nombrePlatoEscolarMiercoles3;
    //TextViews Jueves
    private TextView nombrePlatoEscolarJueves1, nombrePlatoEscolarJueves2, nombrePlatoEscolarJueves3;
    //TextViews Viernes
    private TextView nombrePlatoEscolarViernes1, nombrePlatoEscolarViernes2, nombrePlatoEscolarViernes3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_escolar);

        //Assignacion TextViews Lunes
        nombrePlatoEscolarLunes1 = (TextView) findViewById(R.id.platoEscolarLunes1);
        nombrePlatoEscolarLunes2 = (TextView) findViewById(R.id.platoEscolarLunes2);
        nombrePlatoEscolarLunes3 = (TextView) findViewById(R.id.platoEscolarLunes3);

        //Assignacion TextViews Martes
        nombrePlatoEscolarMartes1 = (TextView) findViewById(R.id.platoEscolarMartes1);
        nombrePlatoEscolarMartes2 = (TextView) findViewById(R.id.platoEscolarMartes2);
        nombrePlatoEscolarMartes3 = (TextView) findViewById(R.id.platoEscolarMartes3);
        //Assignacion TextViews Miercoles
        nombrePlatoEscolarMiercoles1 = (TextView) findViewById(R.id.platoEscolarMiercoles1);
        nombrePlatoEscolarMiercoles2 = (TextView) findViewById(R.id.platoEscolarMiercoles2);
        nombrePlatoEscolarMiercoles3 = (TextView) findViewById(R.id.platoEscolarMiercoles3);

        //Assignacion TextViews Jueves
        nombrePlatoEscolarJueves1 = (TextView) findViewById(R.id.platoEscolarJueves1);
        nombrePlatoEscolarJueves2 = (TextView) findViewById(R.id.platoEscolarJueves2);
        nombrePlatoEscolarJueves3 = (TextView) findViewById(R.id.platoEscolarJueves3);

        //Assignacion TextViews Viernes
        nombrePlatoEscolarViernes1 = (TextView) findViewById(R.id.platoEscolarViernes1);
        nombrePlatoEscolarViernes2 = (TextView) findViewById(R.id.platoEscolarViernes2);
        nombrePlatoEscolarViernes3 = (TextView) findViewById(R.id.platoEscolarViernes3);

    }

    String centro ;
    @Override
    protected void onStart() {
        super.onStart();

        //OBTENEMOS EL STRING QUE ES EL CENTRO ESCOLAR QUE VIENE DE LA OTRA ACTIVITY
        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");

        //Referencia General
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();

        // Referencias Lunes
        DatabaseReference primeroLunes = ref.child(centro).child("MenuEscolar").child("Lunes").child("Primero");
        DatabaseReference segundoLunes = ref.child(centro).child("MenuEscolar").child("Lunes").child("Segundo");
        DatabaseReference postreLunes = ref.child(centro).child("MenuEscolar").child("Lunes").child("Postre");

        // Referencias Martes
        DatabaseReference primeroMartes = ref.child(centro).child("MenuEscolar").child("Martes").child("Primero");
        DatabaseReference segundoMartes = ref.child(centro).child("MenuEscolar").child("Martes").child("Segundo");
        DatabaseReference postreMartes = ref.child(centro).child("MenuEscolar").child("Martes").child("Postre");

        // Referencias Miercoles
        DatabaseReference primeroMiercoles = ref.child(centro).child("MenuEscolar").child("Miercoles").child("Primero");
        DatabaseReference segundoMiercoles = ref.child(centro).child("MenuEscolar").child("Miercoles").child("Segundo");
        DatabaseReference postreMiercoles = ref.child(centro).child("MenuEscolar").child("Miercoles").child("Postre");

        // Referencias Jueves
        DatabaseReference primeroJueves = ref.child(centro).child("MenuEscolar").child("Jueves").child("Primero");
        DatabaseReference segundoJueves = ref.child(centro).child("MenuEscolar").child("Jueves").child("Segundo");
        DatabaseReference postreJueves = ref.child(centro).child("MenuEscolar").child("Jueves").child("Postre");

        // Referencias Viernes
        DatabaseReference primeroViernes = ref.child(centro).child("MenuEscolar").child("Viernes").child("Primero");
        DatabaseReference segundoViernes = ref.child(centro).child("MenuEscolar").child("Viernes").child("Segundo");
        DatabaseReference postreViernes = ref.child(centro).child("MenuEscolar").child("Viernes").child("Postre");


        //Seters Lunes
        primeroLunes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarLunes1.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        segundoLunes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarLunes2.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        postreLunes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarLunes3.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        //Seters Martes
        primeroMartes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarMartes1.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        segundoMartes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarMartes2.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        postreMartes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarMartes3.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        //Seters Miercoles
        primeroMiercoles.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarMiercoles1.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        segundoMiercoles.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarMiercoles2.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        postreMiercoles.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarMiercoles3.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        //Seters Jueves
        primeroJueves.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarJueves1.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        segundoJueves.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarJueves2.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        postreJueves.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarJueves3.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        //Seters Viernes
        primeroViernes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarViernes1.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        segundoViernes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarViernes2.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        postreViernes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoEscolarViernes3.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
}
