package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MiercolesActivity extends AppCompatActivity {
    private TextView nombrePlatoMiercoles;
    private TextView nombrePostreMiercoles;
    Button bElaboracionPrimero, bElaboracionPostre;

    String centro;
    DatabaseReference ref ;
    DatabaseReference primeroMiercoles ;
    DatabaseReference postreMiercoles ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miercoles);
        nombrePlatoMiercoles = (TextView) findViewById(R.id.primerPlatoMiercoles);
        nombrePostreMiercoles = (TextView) findViewById(R.id.postreMiercoles);
        bElaboracionPrimero= (Button) findViewById(R.id.miercolesElaboracionPrimero);
        bElaboracionPostre = (Button) findViewById(R.id.miercolesElaboracionPostre);
    }


    @Override
    protected void onStart() {

        super.onStart();

        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");


        //OBTENER REFERENCIA A LA RAIZ
       ref = FirebaseDatabase.getInstance().getReference();
       primeroMiercoles = ref.child(centro).child("MenuRecomendado").child("Miercoles").child("Primero").child("Plato");
       postreMiercoles = ref.child(centro).child("MenuRecomendado").child("Miercoles").child("Postre").child("Plato");

        primeroMiercoles.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoMiercoles.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });
        postreMiercoles.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePostreMiercoles.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });

        bElaboracionPrimero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MiercolesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton",bElaboracionPrimero.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
        bElaboracionPostre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MiercolesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton",bElaboracionPostre.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });

    }
}