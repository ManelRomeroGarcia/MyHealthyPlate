package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;


public class MainActivity extends AppCompatActivity {
    //Firebase Autentificacion Files
    FirebaseAuth mAuth;
    DatabaseReference mDataBaseRef;

    //Strings FireBase
    String nombre, contrasena;

    //Views and Widgets
    Button botonLog;
    private EditText name, password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        //Obtengo la instancia de Firebase
        mAuth = FirebaseAuth.getInstance();

        //ASSIGNANDO ID'S
        botonLog = (Button) findViewById(R.id.button2);
        name = (EditText) findViewById(R.id.name);
        password = (EditText) findViewById(R.id.password);



        //ON CLICK LISTENER
        botonLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                login();

            }
        });


    }

    private void login() {

        //Guardar el contenido de el Edit Text de NAME Y PASSWORD como tipo string en NOMBRE y CONTRASENA
        nombre = name.getText().toString().trim();
        contrasena = password.getText().toString().trim();
        //Toast.makeText(this, " Bienvenido " + nombre, Toast.LENGTH_LONG).show();



        //OBTENGO LA INSTANCE PARA PODER ACCEDER A LA AUTENTIFICACION DE FIREBASE
        mAuth = FirebaseAuth.getInstance();

        //Comprovar que los campos no esten vacios
        if (TextUtils.isEmpty(nombre) || TextUtils.isEmpty(contrasena)) {
            Toast.makeText(this, "Los campos no pueden estar vacios", Toast.LENGTH_LONG).show();
        } else {
            //USAMOS PARA PODER INICIAR SESION CON EL NOMBRE Y CONTRASEÑA
            mAuth.signInWithEmailAndPassword(nombre, contrasena)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()){
                        //OBTENEMOS EL DOMINIO DEL CORREO ELECTRONICO
                        String[] parts = nombre.split("[@.]");
                        String dominio = parts[1];
                        Log.d("dominio", dominio);

                        //PASAMOS DE LA PANTALLA LOGIN A LA SIGUIENTE PANTALLA
                        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                        intent.putExtra("Centro", dominio);
                        startActivity(intent);


                    }

                }
            });
            /*if (mAuth.getCurrentUser() != null) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);

            }*/

        }
    }
}







