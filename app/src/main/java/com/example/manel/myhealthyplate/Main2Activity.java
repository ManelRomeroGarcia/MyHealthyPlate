package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Button mE = (Button) findViewById(R.id.menuEscolar);
        Button mR = (Button) findViewById(R.id.bmenu);
        Button mP = (Button) findViewById(R.id.bmrec);
        final String centro;
        //OBTENEMOS EL STRING QUE ES EL CENTRO ESCOLAR QUE VIENE DE LA OTRA ACTIVITY
        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");
        Log.d("Main2", centro);
        mR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
                intent.putExtra("Centro", centro);
                Log.d("Main2to3", centro);
                startActivity(intent);

            }
        });
        mE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main2Activity.this, MenuEscolar.class);
                intent.putExtra("Centro", centro);
                Log.d("Main2toM", centro);
                startActivity(intent);

            }
        });
        mP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Main2Activity.this, SolucitudMenuPersonalizadoActivity.class);
                startActivity(intent);
            }
        });
    }
}
