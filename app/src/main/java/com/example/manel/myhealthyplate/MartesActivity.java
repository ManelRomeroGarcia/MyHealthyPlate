package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MartesActivity extends AppCompatActivity {
    private TextView nombrePlatoMartes;
    private TextView nombrePostreMartes;
    Button bElaboracionPrimero, bElaboracionPostre;

    String centro;
    DatabaseReference ref;
    DatabaseReference primeroMartes;
    DatabaseReference postreMartes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_martes);
        nombrePlatoMartes = (TextView) findViewById(R.id.primerPlatoMartes);
        nombrePostreMartes = (TextView) findViewById(R.id.postreMartes);
        bElaboracionPrimero= (Button) findViewById(R.id.martesElaboracionPrimero);
        bElaboracionPostre = (Button) findViewById(R.id.martesElaboracionPostre);
    }



    @Override
        protected void onStart() {
            super.onStart();

            Intent intent = getIntent();
            centro = intent.getExtras().getString("Centro");

            //OBTENER REFERENCIA A LA RAIZ
             ref = FirebaseDatabase.getInstance().getReference();
             primeroMartes = ref.child(centro).child("MenuRecomendado").child("Martes").child("Primero").child("Plato");
             postreMartes = ref.child(centro).child("MenuRecomendado").child("Martes").child("Postre").child("Plato");

            primeroMartes.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String value = dataSnapshot.getValue(String.class);
                    nombrePlatoMartes.setText(value);
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    databaseError.toException();
                }
            });
            postreMartes.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String value = dataSnapshot.getValue(String.class);
                    nombrePostreMartes.setText(value);
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {
                    databaseError.toException();
                }
            });
            bElaboracionPrimero.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MartesActivity.this, ElaboracionActivity.class);
                    intent.putExtra("idBoton",bElaboracionPrimero.getId());
                    intent.putExtra("Centro",centro);
                    startActivity(intent);
                }
            });
            bElaboracionPostre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(MartesActivity.this, ElaboracionActivity.class);
                    intent.putExtra("idBoton",bElaboracionPostre.getId());
                    intent.putExtra("Centro",centro);
                    startActivity(intent);
                }
            });


        }
    }