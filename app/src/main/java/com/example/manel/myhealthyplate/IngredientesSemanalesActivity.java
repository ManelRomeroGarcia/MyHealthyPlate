package com.example.manel.myhealthyplate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;

import static com.google.firebase.auth.FirebaseAuth.getInstance;

public class IngredientesSemanalesActivity extends AppCompatActivity {
    //OBTENER REFERENCIA A LA RAIZ
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
    DatabaseReference ingredientes;
    ArrayList<Ingrediente> selectedItems = new ArrayList<>();
    String[] items;
    ListView ch1;
    SharedPreferences preferences;
    String itemsString;
    String centro;
    ArrayList<Ingrediente> ingredientesList;
    CheckBox box;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences("ItemsSelectedFromUser", Context.MODE_PRIVATE);
        setContentView(R.layout.activity_ingredientes_semanales);
        ch1 = (ListView) findViewById(R.id.checkeable_list);


                /*String selectedItem = ((TextView) view).getText().toString();
                boolean estado = true;
                if (selectedItems.contains(selectedItem)) {
                    selectedItems.remove(selectedItem);
                } else {
                    Ingrediente ingrediente = new Ingrediente(selectedItem,estado);
                    selectedItems.add(ingrediente);

                }

                SharedPreferences.Editor editor = preferences.edit();
                Gson gson = new Gson();
                itemsString = gson.toJson(selectedItems);
                Log.d("valueFromPreferences", itemsString.toString());
                Toast.makeText(IngredientesSemanalesActivity.this,"fghjklñ",Toast.LENGTH_LONG).show();
                editor.putString(FirebaseAuth.getInstance().getCurrentUser().getUid(), itemsString);
                Log.d("user", FirebaseAuth.getInstance().getCurrentUser().getUid());
                editor.apply();*/




        //  Log.d("PEPITO", ingredientes.toString());
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        SharedPreferences prefs = getSharedPreferences("ARRAY_INGREDIENTES", MODE_PRIVATE);
//        String restoredString = prefs.getString(FirebaseAuth.getInstance().getCurrentUser().getUid(), null);
//        if (restoredString != null) {
//            JsonParser parser = new JsonParser();
//            JsonElement tradeElement = parser.parse(restoredString);
//            JsonArray jsonArrayIngredients = tradeElement.getAsJsonArray();
//            if (jsonArrayIngredients != null) {
//                for (int i=0;i<jArray.length();i++){
//                    listdata.add(jArray.getString(i));
//                }
//            }
//        }
    }

    private void comprobarCheckeados() {
        if (preferences != null) {
            Log.d("values4", "oooo");


            String valueFromPreferences = preferences.getString(FirebaseAuth.getInstance().getCurrentUser().getUid(), null);
            Log.d("values", valueFromPreferences.toString());

            Gson gson = new Gson();

            ArrayList values = gson.fromJson(valueFromPreferences, ArrayList.class);

            for (int i = 0; i < values.size(); i++) {

                Log.d("values2", values.toString());

                if (selectedItems.contains(values.get(i).toString())) {

                    Log.d("selectedItems", values.get(i).toString());
                    selectedItems.get(i).getEstado();


                }
            }


        }
    }


    private void init() {
        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");
        ingredientes = ref.child(centro).child("Ingredientes Semanales");
        ingredientesList = new ArrayList<>();

        //Seters Lunes
        ingredientes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    if (preferences != null) {
                        Log.d("values4", "oooo");
                        String valueFromPreferences = preferences.getString(FirebaseAuth.getInstance().getCurrentUser().getUid(), null);
                        if(valueFromPreferences==null) {
                            String value = dataSnapshot.getValue(String.class);
                            if (value != null) {
                                items = value.split(",");


                                ArrayList<String> itemsList = new ArrayList<>(Arrays.asList(items));

                                for (String name : itemsList) {
                                    Ingrediente ingrediente = new Ingrediente(name, false);
                                    ingredientesList.add(ingrediente);

                                }
                                SharedPreferences.Editor editor = preferences.edit();
                                Gson gson = new Gson();
                                itemsString = gson.toJson(ingredientesList);
                                editor.putString(getInstance().getCurrentUser().getUid(), itemsString);
                                editor.apply();
                            }
                        }else{


                                Gson gson = new Gson();
                                ArrayList values = gson.fromJson(valueFromPreferences, ArrayList.class);
                                    Log.d("values4", "oooo");

                                    for (int j = 0; j < values.size(); j++) {
                                        String separaIgua = values.get(j).toString();
                                        String boleano=separaIgua.split("=")[1];

                                        Log.d("values29", String.valueOf(values.size()));
                                        Ingrediente ingrediente=new Ingrediente(separaIgua.split("[=}]")[2],Boolean.parseBoolean(boleano.split(",")[0]));

                                        ingredientesList.add(ingrediente);
                                    }
                            }


                    }
                    IngredienteAdapter adapter = new IngredienteAdapter(ingredientesList, getApplicationContext(),getSharedPreferences("ItemsSelectedFromUser", Context.MODE_PRIVATE));
                    ch1.setAdapter(adapter);
                    //comprobarCheckeados();
                }


            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



}


