package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        //Boton redireccional Lunes
        Button bLunes = (Button) findViewById(R.id.lunes);
        Button bMartes = (Button) findViewById(R.id.martes);
        Button bMiercoles = (Button) findViewById(R.id.miercoles);
        Button bJueves = (Button) findViewById(R.id.jueves);
        Button bViernes = (Button) findViewById(R.id.viernes);
        Button bIngredientes = (Button) findViewById(R.id.ingredientesSem);
        final String centro;

        //OBTENEMOS EL STRING QUE ES EL CENTRO ESCOLAR QUE VIENE DE LA OTRA ACTIVITY
        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");
        Log.d("Main3", centro);
        bLunes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentL = new Intent(Main3Activity.this, LunesActivity.class);
                intentL.putExtra("Centro", centro);
                startActivity(intentL);
            }
        });
        //Boton redireccional Martes
        //Button bMartes = (Button) findViewById(R.id.MartesActivity);
        bMartes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentM = new Intent(Main3Activity.this, MartesActivity.class);
                intentM.putExtra("Centro", centro);
                startActivity(intentM);
            }
        });
        //Boton redireccional Miercoles
        //Button bMiercoles = (Button) findViewById(R.id.MiercolesActivity);
        bMiercoles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMie = new Intent(Main3Activity.this, MiercolesActivity.class);
                intentMie.putExtra("Centro", centro);
                startActivity(intentMie);
            }
        });
        //Boton redireccional Jueves
        //Button bJueves = (Button) findViewById(R.id.JuevesActivity);
        bJueves.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentJ = new Intent(Main3Activity.this, JuevesActivity.class);
                intentJ.putExtra("Centro", centro);
                startActivity(intentJ);
            }
        });
        //Boton redireccional Viernes
        //Button bViernes = (Button) findViewById(R.id.ViernesActivity);
        bViernes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentV = new Intent(Main3Activity.this, ViernesActivity.class);
                intentV.putExtra("Centro", centro);
                startActivity(intentV);
            }
        });


       bIngredientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intenting = new Intent(Main3Activity.this, IngredientesSemanalesActivity.class);
                intenting.putExtra("Centro", centro);
                startActivity(intenting);
            }
        });
    }
}
