package com.example.manel.myhealthyplate;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Objects;

import static com.google.firebase.auth.FirebaseAuth.getInstance;

public class IngredienteAdapter extends BaseAdapter {
    ArrayList<Ingrediente> ingredientes;
    Context context;
    SharedPreferences preferences;


    public IngredienteAdapter(ArrayList<Ingrediente> ingredientes, Context context, SharedPreferences preferences) {
        this.ingredientes = ingredientes;
        this.context = context;
        this.preferences=preferences;
    }

    @Override
    public int getCount() {
        return ingredientes.size();
    }

    @Override
    public Object getItem(int i) {
        return ingredientes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    String itemsString;
    String boleano;
    ArrayList<Ingrediente> boleanos= new ArrayList();
    String valueFromPreferences;

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.semanarow, viewGroup, false);

            final TextView textView = convertView.findViewById(R.id.ingrediente_nombre);
            CheckBox checkBox = convertView.findViewById(R.id.ingrediente_box);

            FirebaseAuth firebaseAuth=getInstance();
            String uid=firebaseAuth.getCurrentUser().getUid();
            valueFromPreferences = preferences.getString(uid, null);

            Gson gson = new Gson();
            ArrayList values = gson.fromJson(valueFromPreferences, ArrayList.class);

            if (preferences != null) {
                Log.d("values4", "oooo");



                Log.d("values21", valueFromPreferences.toString());



                for (int j = 0; j < values.size(); j++) {
                    String separaIgua = values.get(j).toString();
                    boleano=separaIgua.split("=")[1];

                    Log.d("values20", String.valueOf(values.size()));
                    Ingrediente ingrediente=new Ingrediente(separaIgua.split("[=}]")[2],Boolean.parseBoolean(boleano.split(",")[0]));

                    boleanos.add(ingrediente);
                }
            }
            textView.setText(boleanos.get(i).getNombre());
            checkBox.setChecked(boleanos.get(i).getEstado());










            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    String selectedItem = textView.getText().toString();
                    boolean estado = true;


                    boolean entrado = false;
                        for (int i = 0; i < ingredientes.size(); i++) {
                            if (entrado==false ){

                                if (ingredientes.get(i).getNombre().equals(selectedItem) && ingredientes.get(i).getEstado().equals(true)) {
                                    entrado= true;
                                    ingredientes.remove(i);
                                    estado=false;
                                    Ingrediente ingrediente = new Ingrediente(selectedItem, estado);
                                    ingredientes.add(ingrediente);

                                }else  if (ingredientes.get(i).getNombre().equals(selectedItem) && ingredientes.get(i).getEstado().equals(false)) {
                                    entrado = true;
                                    ingredientes.remove(i);
                                    estado=true;
                                    Ingrediente ingrediente = new Ingrediente(selectedItem, estado);
                                    ingredientes.add(ingrediente);

                                }
                            }

                            Log.d("Ingredientes4", ingredientes.get(i).getNombre().toString() + ingredientes.get(i).getEstado().toString());
                        }
                        preferences = context.getSharedPreferences("ItemsSelectedFromUser", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        Gson gson = new Gson();
                        itemsString = gson.toJson(ingredientes);
                        Log.d("valueFromPreferences", itemsString.toString());
                        Log.d("Ingredientes", ingredientes.get(0).toString());
                        // Toast.makeText(IngredientesSemanalesActivity.this,"fghjklñ",Toast.LENGTH_LONG).show();
                        editor.putString(getInstance().getCurrentUser().getUid(), itemsString);
                        Log.d("user", getInstance().getCurrentUser().getUid());
                        editor.apply();
                    }
                        // HASTA AQUI ALFINAL TODO OK

            });

            //TODO mirar aqui
//            final int position = i;
//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                    new IngredientesSemanalesActivity().onCheckChanged(position, b);
//                }
//            });

        }

        return convertView;
    }
}
