package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class JuevesActivity extends AppCompatActivity {
    private TextView nombrePlatoJueves;
    private TextView nombrePostreJueves;
    Button bElaboracionPrimero, bElaboracionPostre;

    String centro;
    DatabaseReference ref;
    DatabaseReference primeroJueves;
    DatabaseReference postreJueves;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jueves);
        nombrePlatoJueves = (TextView) findViewById(R.id.primerPlatoJueves);
        nombrePostreJueves = (TextView) findViewById(R.id.postreJueves);
        bElaboracionPrimero = (Button) findViewById(R.id.juevesElaboracionPrimero);
        bElaboracionPostre = (Button) findViewById(R.id.juevesElaboracionPostre);
    }

    @Override
    protected void onStart() {

        super.onStart();
        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");


        //OBTENER REFERENCIA A LA RAIZ
        ref = FirebaseDatabase.getInstance().getReference();
        primeroJueves = ref.child(centro).child("MenuRecomendado").child("Jueves").child("Primero").child("Plato");
        postreJueves = ref.child(centro).child("MenuRecomendado").child("Jueves").child("Postre").child("Plato");

        primeroJueves.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoJueves.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });
        postreJueves.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePostreJueves.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });

        bElaboracionPrimero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JuevesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton", bElaboracionPrimero.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
        bElaboracionPostre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JuevesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton", bElaboracionPostre.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
    }
}