package com.example.manel.myhealthyplate;

public class Ingrediente {
    String nombre;
    Boolean estado;

    public Ingrediente(String nombre, Boolean estado1) {
        this.nombre = nombre;
        this.estado = estado1;
    }

    public String getNombre() {
        return nombre;
    }

    public Boolean getEstado() {
        return this.estado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}
