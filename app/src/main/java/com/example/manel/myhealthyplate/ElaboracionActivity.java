package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ElaboracionActivity extends AppCompatActivity {
    private TextView ingredientes, paso1;
    //OBTENER REFERENCIA A LA RAIZ
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
    String centro;

    //TextViews
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elavoracion);

        //Setteamos todos los campos TextView para rellenar
        ingredientes = findViewById(R.id.listaIngredientes);
        paso1 = findViewById(R.id.pasoUno);


    }

    @Override
    protected void onStart() {
        super.onStart();

        //Obtencion del contenido del Intntent con la ID del boton presionado
        Intent myIntent = getIntent();
        int id = myIntent.getIntExtra("idBoton", 0);

//        Intent intent = getIntent();
        centro = myIntent.getExtras().getString("Centro");


        //LUNES
        DatabaseReference lunesIngredientesPrimero = ref.child(centro).child("MenuRecomendado").child("Lunes").child("Primero").child("Ingredientes");
        DatabaseReference lunesIngredientesPostre = ref.child(centro).child("MenuRecomendado").child("Lunes").child("Postre").child("Ingredientes");
        DatabaseReference paso1Lunes = ref.child(centro).child("MenuRecomendado").child("Lunes").child("Primero").child("Elaboracion").child("paso1");
        DatabaseReference paso2Lunes = ref.child(centro).child("MenuRecomendado").child("Lunes").child("Postre").child("Elaboracion").child("paso1");
        //MARTES
        DatabaseReference martesIngredientesPrimero = ref.child(centro).child("MenuRecomendado").child("Martes").child("Primero").child("Ingredientes");
        DatabaseReference martesIngredientesPostre = ref.child(centro).child("MenuRecomendado").child("Martes").child("Postre").child("Ingredientes");
        DatabaseReference paso1Martes = ref.child(centro).child("MenuRecomendado").child("Martes").child("Primero").child("Elaboracion").child("paso1");
        DatabaseReference paso2Martes = ref.child(centro).child("MenuRecomendado").child("Martes").child("Postre").child("Elaboracion").child("paso1");
        //MIERCOLES
        DatabaseReference miercolesIngredientesPrimero = ref.child(centro).child("MenuRecomendado").child("Miercoles").child("Primero").child("Ingredientes");
        DatabaseReference miercolesIngredientesPostre = ref.child(centro).child("MenuRecomendado").child("Miercoles").child("Postre").child("Ingredientes");
        DatabaseReference paso1Miercoles = ref.child(centro).child("MenuRecomendado").child("Miercoles").child("Primero").child("Elaboracion").child("paso1");
        DatabaseReference paso2Miercoles = ref.child(centro).child("MenuRecomendado").child("Miercoles").child("Postre").child("Elaboracion").child("paso1");
        //JUEVES
        DatabaseReference juevesIngredientesPrimero = ref.child(centro).child("MenuRecomendado").child("Jueves").child("Primero").child("Ingredientes");
        DatabaseReference juevesIngredientesPostre = ref.child(centro).child("MenuRecomendado").child("Jueves").child("Postre").child("Ingredientes");
        DatabaseReference paso1Jueves = ref.child(centro).child("MenuRecomendado").child("Jueves").child("Primero").child("Elaboracion").child("paso1");
        DatabaseReference paso2Jueves = ref.child(centro).child("MenuRecomendado").child("Jueves").child("Postre").child("Elaboracion").child("paso1");
        //VIERNES
        DatabaseReference viernesIngredientesPrimero = ref.child(centro).child("MenuRecomendado").child("Viernes").child("Primero").child("Ingredientes");
        DatabaseReference viernesIngredientesPostre = ref.child(centro).child("MenuRecomendado").child("Viernes").child("Postre").child("Ingredientes");
        DatabaseReference paso1Viernes = ref.child(centro).child("MenuRecomendado").child("Viernes").child("Primero").child("Elaboracion").child("paso1");
        DatabaseReference paso2Viernes = ref.child(centro).child("MenuRecomendado").child("Viernes").child("Postre").child("Elaboracion").child("paso1");


        switch (id) {
            case R.id.lunesElaboracionPrimero:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                lunesIngredientesPrimero.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL LUNES
                paso1Lunes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;

            case R.id.lunesElaboracionPostre:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                lunesIngredientesPostre.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL LUNES
                paso2Lunes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;

            //TODO RELACIONADO CON EL MARTES

            case R.id.martesElaboracionPrimero:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                martesIngredientesPrimero.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL MARTES
                paso1Martes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;


            case R.id.martesElaboracionPostre:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                martesIngredientesPostre.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL MARTES
                paso2Martes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;

            //TODO RELACIONADO CON EL MIERCOLES

            case R.id.miercolesElaboracionPrimero:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                miercolesIngredientesPrimero.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL MIERCOLES
                paso1Miercoles.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;


            case R.id.miercolesElaboracionPostre:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                miercolesIngredientesPostre.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL MIERCOLES
                paso2Miercoles.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;

            //TODO RELACIONADO CON EL JUEVES

            case R.id.juevesElaboracionPrimero:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                juevesIngredientesPrimero.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL JUEVES
                paso1Jueves.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;


            case R.id.juevesElaboracionPostre:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                juevesIngredientesPostre.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL JUEVES
                paso2Jueves.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;

            //TODO RELACIONADO CON EL VIERNES

            case R.id.viernesElaboracionPrimero:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                viernesIngredientesPrimero.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL VIERNES
                paso1Viernes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;


            case R.id.viernesElaboracionPostre:
                //ponemos las cosas en su sitio es decir los ingredientes del primero del LunesActivity y su elaboracion
                viernesIngredientesPostre.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        ingredientes.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //PASO1 DEL VIERNES
                paso2Viernes.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue(String.class);
                        paso1.setText(value);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
                break;

            default:
                break;
        }

    }
}
