package com.example.manel.myhealthyplate;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LunesActivity extends AppCompatActivity {

    private TextView nombrePlato;
    private TextView nombrePostre;
    Button bElaboracionPrimero, bElaboracionPostre;
    String centro;
    DatabaseReference primero;
    DatabaseReference postre;
    DatabaseReference ref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunes);
        nombrePlato = (TextView) findViewById(R.id.platococo);
        nombrePostre=(TextView)findViewById(R.id.platoPostre);
        bElaboracionPrimero= (Button) findViewById(R.id.lunesElaboracionPrimero);
        bElaboracionPostre = (Button) findViewById(R.id.lunesElaboracionPostre);
    }




    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");


        //OBTENER REFERENCIA A LA RAIZ
        ref = FirebaseDatabase.getInstance().getReference();
        primero = ref.child(centro).child("MenuRecomendado").child("Lunes").child("Primero").child("Plato");
        postre = ref.child(centro).child("MenuRecomendado").child("Lunes").child("Postre").child("Plato");

        primero.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlato.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            databaseError.toException();
            }
        });
        postre.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePostre.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });

        bElaboracionPrimero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LunesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton",bElaboracionPrimero.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
        bElaboracionPostre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LunesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton",bElaboracionPostre.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
    }
}
