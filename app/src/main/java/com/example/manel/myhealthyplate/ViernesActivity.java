package com.example.manel.myhealthyplate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ViernesActivity extends AppCompatActivity {
    private TextView nombrePlatoViernes;
    private TextView nombrePostreViernes;
    Button bElaboracionPrimero, bElaboracionPostre;

    String centro;
    DatabaseReference ref;
    DatabaseReference primeroViernes;
    DatabaseReference postreViernes;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viernes);
        nombrePlatoViernes = (TextView) findViewById(R.id.primerPlatoViernes);
        nombrePostreViernes = (TextView) findViewById(R.id.postreViernes);
        bElaboracionPrimero= (Button) findViewById(R.id.viernesElaboracionPrimero);
        bElaboracionPostre = (Button) findViewById(R.id.viernesElaboracionPostre);
    }

    @Override
    protected void onStart() {

        super.onStart();

        Intent intent = getIntent();
        centro = intent.getExtras().getString("Centro");

        //OBTENER REFERENCIA A LA RAIZ
        ref = FirebaseDatabase.getInstance().getReference();
        primeroViernes = ref.child(centro).child("MenuRecomendado").child("Viernes").child("Primero").child("Plato");
        postreViernes = ref.child(centro).child("MenuRecomendado").child("Viernes").child("Postre").child("Plato");

        primeroViernes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePlatoViernes.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });
        postreViernes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                nombrePostreViernes.setText(value);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                databaseError.toException();
            }
        });

        bElaboracionPrimero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViernesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton",bElaboracionPrimero.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
        bElaboracionPostre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViernesActivity.this, ElaboracionActivity.class);
                intent.putExtra("idBoton",bElaboracionPostre.getId());
                intent.putExtra("Centro",centro);
                startActivity(intent);
            }
        });
    }
}