package com.example.manel.myhealthyplate;

public interface OnCheckChanged {
    void onCheckChanged(int position, boolean isCheck);
}
